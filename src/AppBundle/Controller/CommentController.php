<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Photo;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    /**
     * @Route("/photo-{photo_id}/new-comment")
     * @Method("POST")
     * @param Request $request
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, int $photo_id)
    {
        $comment = new Comment();
        $photo = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->find($photo_id);

        $comment_form = $this->createForm(CommentType::class, $comment);
        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setUser($this->getUser());
            $comment->setPhoto($photo);

            $em->persist($comment);
            $em->flush();
        }
        return $this->redirectToRoute('app_site_feed');
    }
}
