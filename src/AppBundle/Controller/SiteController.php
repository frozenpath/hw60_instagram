<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\CommentType;
use AppBundle\Form\DislikeType;
use AppBundle\Form\LikeType;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_site_feed');
        }
        return $this->render('AppBundle:Site:index.html.twig', []);
    }

    /**
     * @Route("/feed")
     */
    public function feedAction()
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findAll();

        $photo_form = $this->createForm(PhotoType::class);
        $comment_form = $this->createForm(CommentType::class);

        $like_form = [];
        $likes_count = [];

        foreach ($photos as $photo) {
            $likes_count[$photo->getId()] = count($photo->getLikeUsers());
            if (!$photo->getLikeUsers()->contains($this->getUser())) {
                $like_form[$photo->getId()] = $this->createForm(LikeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_like', [
                        'photo_id' => $photo->getId()
                    ])
                ])->createView();
            } else {
                $like_form[$photo->getId()] = $this->createForm(DislikeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_dislike', [
                        'photo_id' => $photo->getId()
                    ])
                ])->createView();
            }
        }

        return $this->render('@App/Site/feed.html.twig', array(
            'photo_form' => $photo_form->createView(),
            'photos' => $photos,
            'user' => $user,
            'comment_form' => $comment_form,
            'like_form' => $like_form,
            'like_counts' => $likes_count
        ));
    }

    /**
     * @Method({"POST", "GET"})
     * @Route("/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $photo = new Photo();

        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photo->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }
        return $this->redirectToRoute('app_site_feed');
    }
}
