<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\FollowType;
use AppBundle\Form\UnsubscribeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @Route("/users")
     */
    public function allUsers()
    {
        $current_user = $this->getUser();
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        $follow_form = [];

        foreach ($users as $user) {
            if (!$user->getFollowers()->contains($this->getUser())) {
                $follow_form[$user->getId()] = $this->createForm(FollowType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_subscribe', [
                        'followed_id' => $user->getId()
                    ])
                ])->createView();
            } else {
                $follow_form[$user->getId()] = $this->createForm(UnsubscribeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_unsubscribe', [
                        'followed_id' => $user->getId()
                    ])
                ])->createView();
            }
        }

        return $this->render('@App/User/users.html.twig', array(
            'users' => $users,
            'follow_form' => $follow_form,
            'current_user' => $current_user
        ));
    }

    /**
     * @Method("GET")
     * @Route("/user/{user_id}")
     * @param int $user_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(int $user_id)
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        /** @var User $user */

        $profile = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($user_id);

        $followings = count($profile->getFollowings());
        $followers = count($profile->getFollowers());

        $posts = $profile->getPhotos();
        $count_posts = 0;
        foreach ($posts as $post) {
            $count_posts++;
        }

        return $this->render('@App/User/profile.html.twig', array(
            'profile' => $profile,
            'followings' => $followings,
            'followers' => $followers,
            'count_posts' => $count_posts,
            'posts' => $posts
        ));
    }

    /**
     * @Method("POST")
     * @Route("/subscribe/{followed_id}")
     * @param int $followed_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeAction(int $followed_id)
    {
        $follower = $this->getUser();
        if (!$follower) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $followed = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($followed_id);

        $followed->addFollower($follower);
        $follower->addFollowing($followed);

        $em = $this->getDoctrine()->getManager();

        $em->persist($followed);
        $em->persist($follower);
        $em->flush();

        return $this->redirectToRoute('app_user_allusers');
    }

    /**
     * @Method("POST")
     * @Route("/unsubscribe/{followed_id}")
     * @param int $followed_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function unSubscribeAction(int $followed_id)
    {
        $follower = $this->getUser();
        if (!$follower) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $followed = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($followed_id);

        $followed->removeFollower($follower);
        $follower->removeFollowing($followed);

        $em = $this->getDoctrine()->getManager();

        $em->persist($followed);
        $em->persist($follower);
        $em->flush();

        return $this->redirectToRoute('app_user_allusers');
    }

    /**
     * @Method("POST")
     * @Route("/like/{photo_id}")
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function likeAction(int $photo_id)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($photo_id);

        $photo->addLikeUser($user);
        $user->addLikePhoto($photo);

        $em = $this->getDoctrine()->getManager();

        $em->persist($photo);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('app_site_feed');
    }

    /**
     * @Method("POST")
     * @Route("/dislike/{photo_id}")
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function disLikeAction(int $photo_id)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($photo_id);

        $photo->removeLikeUser($user);
        $user->removeLikePhoto($photo);

        $em = $this->getDoctrine()->getManager();

        $em->persist($photo);
        $em->flush();

        return $this->redirectToRoute('app_site_feed');
    }
}
